This diary file is written by Edwin Ke in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Tried to come up with a few mediators, but turns out I was confused with the definition of a mediator.
* Our group didn't get to do the presentation. Next time, we will be faster to grab the chance.
* The layouts of a PowerPoint presentation can really affect its outcome.
* This is my first time using BitBucket. Might take a while to get used to this tool.

# 2021-09-30 #

* Think it would be a bit difficult for every person to present 3 times, since many students have enrolled in this class. 
* Guess I'll just have to be fast and grasp the chance to present.
* There is always going to be negativity on social media, so it's important to learn to not be greatly affected.
* Since I am a highly sensitive person, I definitely need to be aware of the negative effects of social media.
* Gave a presentation and received lots of feedback from the professor. Glad to have learned more about how to make better slides.
* The world is getting better gradually, but even with the problem of famine being solved, I believe we still have a long way to go.
* Learned that one good way to deal with fake news is by comparing alternative sources.
* Statistics can be inaccurate amd misleading if the surveys conducted to produce them are ambiguous.

# 2021-10-07 #

* Gave a presentation regarding why statistics are important today. 
* Was delighted to see that the professor liked my conclusion: "Statistics are important, because they help us love people."
* Learned that the Congress in the United States has been demanding reports on UFOs from government agencies.
* Just because a piece of news about the existence of aliens has been proven fake, we can't rule out the possibility of their existence entirely.
* Did a survey on financial knowledge. Astonished by the fact that there were a bunch of things I didn't know, even though I use money every single day.
* The reason why there is always a surge in the amount of money taken out from banks during winter in Taiwan is because of the traditional custom. 
* There are five desirable properties of money. However, according to the professor, the New Taiwan Dollar is not durable.
* Was surpised to find out that when people make loans, new money is actually created simultaneously.
* The purchasing power of the New Taiwan Dollar has been decreasing. No wonder it is not durable.
* Ｑuantitative Easing is an unconventional policy that may spur economic activity in a short time. However, it has high risks.
* There are eight tasks this week. Better get started.

# 2021-10-14 #
* Felt incredibly frustrated during class today because my Internet connection somehow kept crashing.
* The TED talk videos were fantastic, although I would have enjoyed them a lot more if my WiFi was stronger.
* Have heard of the word "fascism" before but have never understood the meaning of it till this class.
* Genuinely moved by the second TED talk video, in which a man named Christian Picciolini shared his story of getting out of a hatred movement and helping others to do so.
* Agree with a fact mentioned by Christian-When faced with unfamiliar stuff, we tend to fear or even hate it.
* I remember disliking a British singer named Jess Glynne before even listening to her songs, because I had never heard of her before. But after hearing her music, she ended up becoming one of my favorite artists.
* I guess my experience above proves the aforementioned fact.
* Sometimes when we look into the backgrounds of seemingly ill-minded people, we would build up compassion for the hurt they went through.
* If potholes are left neglected or overlooked, they might gradually affect our mentality over time, no matter how hard we try not to think about them.
* People's repulsive behavior often reflects the struggles they have in themselves. For instance, Extremists often feel a lack of belongingness.
* The foundation of one's identity should be established on self-confidence and self-love, instead of hatred towards other people.
* Hopefully we will all learn to have sympathy and love for each other, even with scars over our hearts.