This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Winny Chang H24064064 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* I learned Wright's Law in the first class.（products will become cheaper from year to year）But I wonder if it is based on labor exploitation and environmental pollution. Do we still want it? 
* Fast fashion is an example. Do we need a lot? Do we need to chase the better one forever? 

* Glasl's model of conflict escalation. It let me know communication is important. Only communication can let us have a win-win situation.
* The professor gave other groups a lot of feedback. And I also made some errors they made in my ppt.
* I think I can learn a lot from this course.

# 2021-09-30 #

* I gave a presentation, and got some feedback from the professor. For example, it is not enough to cite the website, you also need to write the author's name and the last time you visited the website.
* My original teammates all dropped the course, and thanks for the TA, now I have two new teammates.
* There are three videos we need to watch this week, and my favorite one is "The best stats you've ever seen" by Hans Rosling. My major is statistics, and after studying for some years, I was bored with statistics, but after watching the video, it rebooted my passion for statistics. I hope I can be a statistician like Hank.
* The videos that professor always gave us always changed my mind a lot. I appreciate the professor's effort to the course and feel lucky to take the course.

# 2021-10-07 #
* I don't know many problems of the quiz that the teacher asked us to answer about the financial system.
* There is income inequality in Taiwan.
* It’s lucky to find my teammate easily this time.
* Although all my teammates are Taiwanese, we still  communicate in English. And it is a good way to practice English.
* I learned that the Gini index is an indicator to measure income inequality. The higher, the more inequality. 
