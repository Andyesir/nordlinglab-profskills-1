This diary file is written by Gabrielle Vania Tandjung H24075332 in the course Professional skills for engineering the third industrial revolution.  

# 2021-09-23 

* The Professor still discussed about conflicts on the second lecture, we were given more examples 
* There were some presentations on the second period, the Professor gave some inputs for improvements 
* The Professor also taught us how to use Github and how to make the diary properly 
* On the third period, the class discussed about the 4th Industrial Revolution that I didn't know about before 

# 2021-09-30 

* This week we talked about how to differentiate fake and real news 
* We also watched a video talking about how to spot bad statistics 
* As a Statistics student myself, that video was very interesting to watch 
* The professor talked again about diary and grouping problems since there are still some students who do not understand, me included 
* My group have not gotten the chance to present yet 

# 2021-10-07 

* This week we talked about economy and finance 
* The professor asked us to fill a questionnaire about economy and filling that made me realized that I have forgotten a lot of things about economy 
* Then we discussed more about the international and global economy situations 
* All of my previous group members dropped this class so I had to do the presentation for week 4 all by myself 
* Thankfully we are going to have new group arrangements starting next week
