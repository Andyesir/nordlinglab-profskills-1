This diary file is written by Edwina Aurelia Saksana E14075142 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23

* First lecture and presentation day.
* I think exponential growth mostly applies to futuristic innovations.
* Exponential growth would stop when newer and better versions of the innovations appear.
* Professor teaches us that doing things according to templates is very important.

# 2021-09-30

* Professor uses Google Meet because it has subtitles feature and I think it's really thoughtful of Professor.
* More presentations, with some mistakes but we've gotten better from previous presentation.
* I learned how to verify news whether it's fake or real.
* It is important to verify news in order to prevent the spreading of rumours.

# 2021-10-7

* I like to learn more about finance, even if I'm majoring in Mechanical Engineering, because I think understanding how finance works is a very important aspect in life, that everyone should understand.
* Though Taiwanese finance is not so important to a foreigner like me, but I think knowing the basic knowledge of the world's finance is also important.
* A country's economy can inflate and deflate depending on debts and loans.
* During this pandemic, many people lose their jobs and have to depend on loans before finding new jobs, making so many countries' economy unstable.