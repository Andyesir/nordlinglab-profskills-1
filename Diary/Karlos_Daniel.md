This diary file is written by Karlos Daniel E14085147 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #
* In the second lecture, the Professor explain more about conflict, that the conflict are divided into a several level. 
* I can learn how to use bitbucket, even though I have a little bit problem to use it for the first time.
* From the TED talk video by Steven Pinker about "Is the world getting better or worse? A look at the numbers". I got that, the world will getting better in the future.

# 2021-09-30 #
* This week lecture is about fake news.
* learn how to spot a bad statistic. (3 ways: 1. Do you see uncertainty in the data? 2. Do you see yourself in the data? 3. How was the data collected?)
* I feel Mona Chalabi talk too fast, so its hard to understand what she saying. but in the end I got the point.
* My team got a chance to present this week.

# 2021-10-07 #
* This week lecture is about money (economy and finance).
* Money and credit are used for trading or transactions. (consist buyers and seller)
* Learned about how money was determined, the value of money, and about economic growth.
* Filling a questionnaire about money and economy in the world.
* Having a new group for presentation starting from next week.
