*This diary file is written by Shu-Wen Guo (郭姝彣) E54091112


# Week3 2021/9/30

* It was my first time in this class. As a result of not attending the the first two lectures, I have some difficulty in understanding what's going on; also,
 attending a all-english class isn;t a piece of cake for me, but I thinl I'll get better gradually.
* I think this class would be a lot better if it is physical. Hopefully we don't have to take online classes whole semester.
* This website is hard to use. It will be a lot simpler if we just use the website NCKU provides.
* From https://newsvoice.se/2021/10/jason-christoff-mental-conditioning/, I learn more about news and ways to avoid taking in fake news.

# Week4 2021/10/07
* I delivered a presentation today. Giving a presentation in a foreign language isn't easy for me; I spent a lot more time to prepare my text of speech than normal.
 Nevertheless, the result is good. I got the chance the show my work, and the presentation went out nicely.
* The topic of finance in this lecture is too hard for me, and the content seemed too much, since we're not able to watch those videos in class. However, this is
 still an nice lecture, I learn some interesting things; for example, how to print a million NTD legally.
 
 # Week4 2021/10/14
 * I am glad that diary reading wasn't included in this week's lecture. Don't take me wrong. I don't hate hearing people sharing their diaries. However, I do think
  it is taking up too much time.
 * The videos we are required to watch is interesting. I find it a little stupid how we humans live in fictions that we create, but it is also fun that we create
  myths for explaining the unknown, and rules for maintaining social orders.